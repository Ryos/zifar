import {EventEmitter} from "events";
import {constrain} from "../../lib/utils/numbers";
import Timeout = NodeJS.Timeout;

type CachedValue<T> = {
  found: true;
  value: T;
}

type CachedValueNotFound = {
  found: false;
}

type CacheMetadata = {
  name: string;
  stale: boolean;
  timeout?: Timeout;
  value: any;
}

type CacheReturn<T> = CachedValue<T> | CachedValueNotFound;

export class Cache extends EventEmitter {
  private static instance: Cache;
  public static MAX_LIFETIME = 8640000000000000;

  protected values: Map<string, CacheMetadata>;

  private constructor() {
    super();

    this.values = new Map();
  }

  protected emitExpiry(key: string, value: any, lifetime: number, removeOnExpiry: boolean) {
    const self = this;

    this.emit(
      key,
      value,
      function update(newValue: any, newLifetime: number = lifetime, newRemoveOnExpiryFlag: boolean = removeOnExpiry) {
        self.put(key, newValue, newLifetime, newRemoveOnExpiryFlag);
      }
    );
  }

  public static getCache() {
    if (!this.instance) {
      this.instance = new Cache();
    }

    return this.instance;
  }

  /**
   * Retrieve a value by key from the cache.
   *
   * @param key The key to retrieve.
   * @returns undefined if the key has no value or the cache is stale, else the cached value.
   */
  public get<T>(key: string): CacheReturn<T> {
    const cachedValue = this.values.get(key);

    return (typeof cachedValue === "object" && !cachedValue.stale)
      ? {found: true, value: cachedValue.value}
      : {found: false};
  }

  /**
   * Store a given value by key in the cache.
   *
   * If a lifetime is specified, the cache will emit an event with the key name when the entry goes stale.
   *
   * @param key The key to store the value under.
   * @param value The value to store in the cache.
   * @param lifetime How long the value should live for, in milliseconds.
   * @param removeOnExpiry Flag to remove the entry from the cache when the value goes stale.
   *
   * @returns an object that allows expiry listeners to be attached to the cache slot.
   */
  public put(
    key: string,
    value: any,
    lifetime: number = 0,
    removeOnExpiry: boolean = false,
  ): ExpiryUtils {
    if (this.values.has(key)) {
      this.remove(key, false);
    }

    const cacheValue = <CacheMetadata>{
      name: key,
      stale: false,
      value: value
    };

    if (Number.isFinite(lifetime) && lifetime > 0) {
      cacheValue.timeout = setTimeout(() => {
        cacheValue.stale = true;

        this.emitExpiry(key, value, lifetime, removeOnExpiry);

        if (removeOnExpiry) {
          this.remove(key);
        }
      }, constrain(0, Cache.MAX_LIFETIME, lifetime));
    }

    this.values.set(key, cacheValue);

    // Cache.getCache() is used to get around the inability to use two "this" bindings.
    return {
      first(listener: ExpiryListener) {
        Cache.getCache().prependListener(key, listener);
        return this;
      },

      firstOnce(listener: ExpiryListener) {
        Cache.getCache().prependOnceListener(key, listener);
        return this;
      },

      next(listener: ExpiryListener) {
        Cache.getCache().on(key, listener);
        return this;
      },

      nextOnce(listener: ExpiryListener) {
        Cache.getCache().once(key, listener);
        return this;
      }
    }
  }

  /**
   * @description Remove a stored value by key from the cache, if it exists. Also cancels the lifetime that has been set
   * on the value if one exists, along with any registered expiry listeners.
   *
   * @param key The key to remove from the database.
   * @param removeListeners Whether to remove expiry listeners from the cache.
   */
  public remove(key: string, removeListeners: boolean = true): void {
    if (this.values.has(key)) {
      let {timeout} = this.values.get(key)!;

      if (timeout) {
        clearTimeout(timeout);
      }
    }

    if (removeListeners) {
      this.removeAllListeners(key);
    }

    this.values.delete(key);
  }
}

/**
 * @see Cache.get
 */
export function get<T>(key: string, cache: Cache): CacheReturn<T> {
  return cache.get(key);
}

/**
 * @see Cache.put
 */
export function put(key: string, value: any, cache: Cache, lifetime: number = 0) {
  return cache.put(key, value, lifetime);
}

/**
 * @see Cache.remove
 */
export function remove(key: string, cache: Cache, removeListeners: boolean = true): void {
  cache.remove(key, removeListeners);
}

/**
 * A shorthand type for an expiry listener.
 */
type ExpiryListener = (value: any, update: UpdateValue) => void;

type ExpiryUtils = {
  /**
   * @description Prepend an expiry listener to the value that will be executed every time it expires. Discouraged for
   * values that will update on expiry, unless an infinite update loop is required.
   */
  first(listener: ExpiryListener): ExpiryUtils;

  /**
   * @description Prepend an expiry listener to the value that will be executed only the next time it expires. Should be
   * preferred for values that will update on expiry, unless an infinite update loop is required.
   */
  firstOnce(listener: ExpiryListener): ExpiryUtils;

  /**
   * @description Append an expiry listener to the value that will be executed every time it expires. Discouraged for
   * values that will update on expiry, unless an infinite update loop is required.
   */
  next(listener: ExpiryListener): ExpiryUtils;

  /**
   * @description Append an expiry listener to the value that will be executed only the next time it expires. Should be
   * preferred for values that will update on expiry, unless an infinite update loop is required.
   */
  nextOnce(listener: ExpiryListener): ExpiryUtils;
}

type UpdateValue = (value: any, lifetime?: number, removeOnExpiry?: boolean) => void;
