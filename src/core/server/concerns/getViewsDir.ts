import {resolve} from "path";

export function getViewsDir(base: string): string[] {
  return [
    resolve(base, "views"),
    resolve(base, "../global/views")
  ];
}
