export interface ViewContext {
  [key: string]: any;

  settings: { [name: string]: any }
  _locals: ViewLocals;

  body: string;
}

interface ViewLocals {
  [name: string]: any;

  scriptBundles: string[];
  stylesheets: string[];
}
