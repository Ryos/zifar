declare global {
  namespace Express {
    interface Request {
      bot: DiscordConnection["client"];
      db: DiscordConnection["db"];
    }
  }
}

import debug from "debug";
import * as express from "express";
import {Application, Request, Response, NextFunction} from "express-serve-static-core";
import * as hbs from "hbs";
import {createServer, Server, ServerOptions} from "http";
import {join, resolve} from "path";
import {registerHelpers} from "./engine/helpers";
import {DiscordConnection} from "../bot";
import {Cache, get, put} from "../objects/Cache";
import {CacheValues} from "../../lib/enum";
import hbsUtils = require("hbs-utils");

const cache = Cache.getCache();

export type ServerConnection = DiscordConnection & {
  server: Server;
}

type ConnectProxy = { getWebserver(connection: DiscordConnection): Promise<ServerConnection> };

type Handle = (number | string);

export function onPort(port: number): ConnectProxy {
  return { getWebserver: getWebserver.bind(null, port) };
}

export function onSocket(sock: string): ConnectProxy {
  return { getWebserver: getWebserver.bind(null, resolve(sock)) };
}

async function getWebserver(handle: Handle, connection: DiscordConnection): Promise<ServerConnection> {
  const server = get<Server>(CacheValues.HTTP_SERVER, cache);

  return (server.found)
    ? {...connection, server: server.value}
    : {...connection, server: await connect(handle, connection)};
}

async function connect(handle: Handle, connection: DiscordConnection): Promise<Server> {
  const log = debug("zifar").extend("http");

  const server = await getServer(connection);

  switch(typeof handle) {
    case "number":
      log("Starting http server on port %s", handle);
      break;
    case "string":
      log("Starting http server at socket %s", handle);
      break;
  }

  return server.listen(handle);
}

async function getServer(connection: DiscordConnection): Promise<Server> {
  const serverSlot = get<Server>(CacheValues.HTTP_SERVER, cache);

  if (serverSlot.found) {
    return serverSlot.value;
  }

  const app = express().set("env", process.env.NODE_ENV || "production");
  const config = await import(`./config/${ app.get("env") }`);
  const handlebars = hbs.create();
  const handlebarsUtils = hbsUtils(handlebars);
  const options = <ServerOptions>{};

  config.apply(app, handlebars, handlebarsUtils);
  registerHelpers(handlebars);

  applyMiddleware(app, {connection});
  app.use("/", require("./features/home/router").router);

  const server = createServer(options, app);

  put(CacheValues.HTTP_SERVER, server, cache);

  return server;
}

function applyMiddleware(app: Application, options: { connection: DiscordConnection }): void {
  app.use("/static", express.static(join(__dirname, "/static"), {
    cacheControl: app.get("cache-control"),
    fallthrough: false,
    maxAge: app.get("cache-max-age")
  }));

  app.use(function setConnections(req: Request, res: Response, next: NextFunction) {
    req.bot = options.connection.client;
    req.db = options.connection.db;

    next();
  });

  app.use(function setStylesheets(req: Request, res: Response, next: NextFunction) {
    res.locals.stylesheets = ["main"];

    next();
  });
}
