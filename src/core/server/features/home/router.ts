import * as express from "express";
import * as HomeController from "./controller";
import {getViewsDir} from "../../concerns/getViewsDir";

const router = express();

router.set("feature.name", "home");
router.set("feature.bundles", "home");
router.set("views", getViewsDir(__dirname));

router.use(HomeController.setBundles);

router.route("/")
  .get(HomeController.index);

export {router};
