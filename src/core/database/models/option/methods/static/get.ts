import {OptionDocument} from "../../interfaces";
import {Model} from "mongoose";
import {isNil} from "../../../../../../lib/utils/language";

export async function getFn<T>(this: Model<OptionDocument>, option: string, fallback: T): Promise<T> {
  return this.findOne()
    .where("name").equals(option)
    .exec()
    .then(option =>
      (!isNil(option) && isNil(option.value))
        ? option.value!
        : fallback
    );
}
