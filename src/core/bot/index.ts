import debug from "debug";
import {Client} from "discord.js";
import {join} from "path";
import {DatabaseConnection} from "../database";
import {CacheValues} from "../../lib/enum";
import {Cache, get, put} from "../objects/Cache";
import {CommandManager} from "../objects/command/CommandManager";
import {CommandRegistry} from "../objects/command/CommandRegistry";

const cache = Cache.getCache();

export type DiscordConnection = DatabaseConnection & {
  client: Client;
}

export async function getDiscordConnection({db}: DatabaseConnection): Promise<DiscordConnection> {
  const instance = get<Client>(CacheValues.DISCORD_CONNECTION, cache);

  return instance.found
    ? {db, client: instance.value}
    : {db, client: await connect({db})}
}

async function connect({db}: DatabaseConnection): Promise<Client> {
  const client = new Client({ fetchAllMembers: true });
  const commandRegistry = new CommandRegistry();
  const commandManager = new CommandManager(commandRegistry, {db, client});
  const log = debug("zifar").extend("bot");

  client.on("disconnect", () => {
    log("Disconnected from Discord API.");
  });

  client.on("error", error => {
    log("There was an error: %s", error);
  });

  client.on("message", commandManager.execute(db));

  client.on("ready", () => {
    log("Connected to Discord API.");
  });

  client.once("ready", async () => {
    log("Loading commands.");

    const count = await commandManager.loadCommands(join(__dirname, "commands"));

    log(`${count} commands loaded successfully.`);
  });

  log("Authenticating with Discord API...");

  await client.login(process.env.ZIFAR_BOT_TOKEN);

  log("Authenticated with Discord API.");

  put(CacheValues.DISCORD_CONNECTION, client, cache);

  return client;
}
