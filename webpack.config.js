const {join} = require("path");

function getScriptDirFor(feature) {
  return join(__dirname, `dist/core/server/features/${feature}/scripts/index.js`);
}

module.exports = {
  entry: {
    "global": getScriptDirFor("global"),
    "home": getScriptDirFor("home")
  },

  mode: "development",

  optimization: {
    splitChunks: {
      chunks: "all"
    }
  },

  output: {
    chunkFilename: "[name].chunk.js",
    filename: "[name].js",
    path: join(__dirname, "dist/core/server/static/js/"),
  },

  target: "web"
};
